#!/bin/bash 

if [ "$1" == "" ]; then 
	echo "You need to specify script name to run"
    	echo "Exiting..."
	exit
fi

trap cleanup INT

cleanup() {
  echo "killing browser_compiler_pid: $browser_compiler_pid"
  kill -15 $browser_compiler_pid 2> /dev/null

  echo "killing browserify-modules.sh"
  kill -15 $BROWSERIFY_PID 2> /dev/null
  wait $browser_compiler_pid
  wait $BROWSERIFY_PID

  kill -9 $browser_compiler_pid 2> /dev/null
  kill -9 $BROWSERIFY_PID 2> /dev/null
  exit
}

echo "Compiling LiveScript to Javascript for browsers..."
lsc -o ./static -cw ./static &
browser_compiler_pid=$!

echo "Browserifying modules..."
./watch.sh ./static/ic/weblib.js ./browserify-modules.sh &
BROWSERIFY_PID=$!

#nodemon --exec lsc $1
./watch.sh . lsc $1

echo "All done, exiting..."
cleanup 
