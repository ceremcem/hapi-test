require! {
  'prelude-ls': {
    flatten
    initial
    drop
    join
    concat
    tail
    head
    map
    zip
    split
    }
  'weblib': {
    mk-realtime-input
    mk-radiobox
    test: weblib-test
    state-of
    }
}

weblib-test!

$ -> $ '#header' .css 'background-color', '#317EAC'

url = window.location.href
arr = url.split "/"
addr_port = arr.0 + "//" + arr.2

socketio-path = [''] ++ (initial (drop 3, arr)) ++ ['socket.io']
socketio-path = join '/' socketio-path

console.log 'socket.io path: ' + socketio-path

socket = io.connect addr_port, path: socketio-path 
  
socket.on "connect", -> 
  console.log "Connected to the server!"

chat-value = 0

socket.on "tweet", (tweet) -> 
  #console.log "tweet from:", tweet.user
  #console.log "contents:", tweet.text
  $ '#messages' .prepend do
    $ '<li>' .text tweet.user + ' says: ' + tweet.text
    
  chat-value := parse-int tweet.text


$ '#submitbutton'
  .click !-> 
    console.log 'tweet should be sent...'
    socket.emit 'tweet', 
      user: 'chat'
      text: $ '#m' .val!          
    new-value = parse-int do 
      $ '#m' .val!

    chat-value := new-value 
    $ '#m' .val ''
  
$ 'document' .ready !->
    $ '#m' .keypress (e) !->
      if e.keyCode == 13   #the enter key code
        $ '#submitbutton' .click!
        console.log "just like clicked"





# We use an inline data source in the example, usually data would
# be fetched from a server

data = []

getRandomData = ->
  totalPoints = 300

  if data.length > 0 then
    data := tail data
    
  /*
  # Do a random walk
  while data.length < totalPoints 
    prev = if data.length > 0 
           then data[data.length - 1] 
           else 50
    y = prev + Math.random() * 10 - 5

    if y < 0
      y = 0
    else if y > 100
      y = 100
    
    data.push y
  */
  while data.length < total-points 
    data.push chat-value
    
  # Zip the generated y values with the x values
  return zip [i for i from 0 to data.length] data

# Set up the control widget
updateInterval = 30

graph-data = -> 
  return 
    * label: 'test'
      data: get-random-data! 
      color: 'red'
    * label: 'test2'
      data: get-random-data!

myplot = $.plot '#placeholder1', graph-data!, 
        series: 
          shadowSize: 0   # Drawing is faster without shadows
        yaxis: 
          min: 0,
          max: 100
        xaxis: 
          show: false
            
get-pie-data = -> 
  return 
    * label: "seri1"
      data: 1
    * label: "seri2"
      data: 2.5
   
#console.log "pie data: ", get-pie-data!

pie-plot = $.plot '#pietest', get-pie-data!, 
        series: 
          pie:
            show: true
            inner-radius: 0.5

update = !-> 
  myplot.set-data graph-data! 
  myplot.draw!

  pie-plot.set-data get-pie-data!
  pie-plot.draw!
  
  setTimeout update, updateInterval

update!


realtime-input-changed3 = (elem) -> 
  !-> 
    socket.emit 'tweet', 
      user: elem.attr 'id'
      text: elem.val!   

mk-realtime-input '.realtime-input', 500, realtime-input-changed3



# treat checkbox element as realtime-input
button-partial-handler = (elem) -> 
  !-> 
    $ '#test_button3_label' .text do
      $ '#test_button3_input' .val!

mk-realtime-input '#test_button3_checkbox', 800, button-partial-handler


radiobox-handler = (elem, event) !-> 
  current-button = elem
  str-join = join ''
  radiobox-message = str-join [
    * (elem.attr 'value') 
    * ' is changed to ' 
    * (state-of elem).to-string!
    ]
  radiobox-gid = elem.data!.'group-id'
  
  debug-message = ''
  debug-message += 'rb-handler: '
  debug-message += radiobox-gid + ': '
  debug-message += 'event.type: ' + event.type
  
  if ((state-of current-button) is not current-button.data!.'old-value'
    and event.type is not 'chain')
    socket.emit do
      * 'tweet'
      * user: radiobox-gid
        text: radiobox-message
  if false
    socket.emit do
      * 'tweet'
      * user: 'debug'
        text: str-join [
          * (elem.attr 'value') 
          * ': '
          * current-button.data!.\old-value
          * ' -> '
          * (state-of current-button).to-string!
          ]
   
  console.log debug-message
  


radiobox-listener-handler = (elem) !->   
  radiobox-gid = elem.data!.'group-id'
  dom-id = elem.attr 'value'
  debug-message1 = ''
  debug-message1 += 'radiobox: ' 
  debug-message1 += 'handler added for '
  debug-message1 += radiobox-gid + '.' + dom-id

  ## debug
  #console.log debug-message1
  
  socket.on do 
    * 'tweet'
    * (tweet) -> 
        # TODO: so many calls made to this handler
        debug-message = ''
        debug-message +=  'rb-receiver: ' 
        debug-message += radiobox-gid + '.' + dom-id
        
        if tweet.user is radiobox-gid
          change-log = split ' is changed to ', tweet.text
          debug-message += ', '
          debug-message += join '->' change-log
          if change-log.0 is dom-id
            check-state = (change-log.1 is 'true')
            elem.prop 'checked', check-state
            
            current-button = elem
            if (state-of current-button) is true
              # uncheck the other buttons
              debug-message += " unchecking other buttons, "
              b = current-button.data!.\next-button        
              until b.data!.'id' is current-button.data!.'id'
                ## debug
                #console.log 'looping through button nodes: ', (b)
                b.prop 'checked', false
                b.trigger 'chain'
                b = b.data!.'next-button'
                
              debug-message += " unchecking other buttons DONE."
            #console.log 'visible state of ' + radiobox-gid + '.' + dom-id + ' changed to ', check-state, elem

        else
          debug-message += ' this msg is NOT for me'
          
        ## debug 
        console.log debug-message
$ '.radiobox' .each -> 
  group = $ this 
  group-id = '#' + (group.attr 'id')
  mk-radiobox group-id, radiobox-handler, radiobox-listener-handler
  #console.log 'radiobox ' + group-id + ' created.'
  
$ '#test-cb-1' .click -> 
  socket.emit do
    * 'tweet'
    * user: 'X'
      text: $ this .prop 'checked'
      
socket.on do
  * 'tweet'
  * (tweet) -> 
      #console.log 'tweet got from Y handler'
      if tweet.user is 'X'
        x-state = tweet.text
        #console.log 'X sends message: ' + x-state.to-string!
        $ '#test-cb-2' .prop 'checked', x-state
        
                

