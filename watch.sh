#!/usr/bin/env bash
# script:  watch
# author:  Mike Smullin <mike@smullindesign.com>
# license: GPLv3
# description:
#   watches the given path for changes
#   and executes a given command when changes occur
# usage:
#   watch <path> <cmd...>
#

path=$1
shift
cmd=$*
sha=0
update_sha() {
  sha=`ls -lR --time-style=full-iso $path | grep  '[html|js|ls]$' | sha1sum`
}
update_sha
previous_sha=$sha
build_pid=-1
build() {
  echo -en " building...\n\n"
  $cmd &
  build_pid=$!
  wait $build_pid
  echo -en "\n--> resumed watching.\n"
}
compare() {
  update_sha
  if [[ $sha != $previous_sha ]] ; then
    echo -n "change detected,"
    if [[ "$build_pid" -ne "-1" ]]; then 
      echo "cleanup..."
      cleanup
    fi
    build
    previous_sha=$sha
  else
    echo -n ''
  fi
}
cleanup() {
  echo "killing previous build command, pid: $build_pid"
  kill -15 $build_pid 2> /dev/null
  wait $build_pid
  kill -9 $build_pid 2> /dev/null
}

finally() {
  cleanup
  exit
}
trap finally SIGINT
trap finally SIGQUIT
trap finally SIGTERM
trap finally SIGTSTP

echo -e  "--> Press Ctrl+C to force build, Ctrl+\\ to exit."
echo -en "--> watching \"$path\"."
while true; do
  compare
  sleep 1
done
