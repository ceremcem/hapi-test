#!/bin/bash 


mkdir -p ./tmp/coffee-watch

echo "Compiling Coffee-script to Javascript..."
lsc -o ./tmp/lsc-watch/ -cw .

echo "Removing temporary directory..."
rm ./tmp -r 

echo "All done, exiting..."
