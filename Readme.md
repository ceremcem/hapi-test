# Install dependencies

## nodejs, npm 
### Debian: 

    apt-get install curl
    curl -sL https://deb.nodesource.com/setup | bash -
    apt-get install node nodejs npm


## LiveScript, prelude.ls and others:

    $ npm install -g LiveScript
    $ cd project-folder/
    $ npm install

# Run: 

Run the server: 

    $ lsc -c ./static -o ./static 
    $ ./browserify-modules.sh
    $ lsc server.ls

  or, for development:

    $ ./run-development.sh server.ls    # monitor changes and restart if a file changes


With the web browser, go to: 

  * http://localhost:3000
  * http://localhost:3000/render
  
  
# Stop ./run-development.sh script

    Ctrl+C, Ctrl+Z


