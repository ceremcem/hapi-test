{map} = require 'prelude-ls'

Path = require "path" 
Hapi = require "hapi" 
server = new Hapi.Server(3000)

io = require('socket.io') server.listener

# Handle socket.io connections

handleClient = (socket) -> 
  socket.emit 'tweet', 
    user: "nodesource added"
    text: "Hello world!"


io.on 'connection', (socket) -> 
  /*
  tweet = 
    user: "node source 22"
    text: "hello world 222" 
      
  
  interval = setInterval (() -> 
    socket.emit 'tweet', tweet
    return), 
    1000
  
  socket.on "disconnect", () ->
    clearInterval interval
    return 
  */
  socket.on "tweet", (tweet) -> 
    console.log "tweet from browser",
      tweet, 
      'broadcasting all others'
    socket.broadcast.emit 'tweet', tweet
    socket.emit 'tweet', tweet
    
    
server.route do
  method: "GET"
  path: "/"
  handler: (request, reply) ->
    reply "Hello, world!"
    return

server.route do
  method: "GET"
  path: "/render"
  handler: (request, reply) ->
    reply.view "template",
      title: "test"
      body:
        a: "test OK"
        b: "naber"

      input_text: "vaaaayyy"

    return

server.route do
  method: "GET"
  path: "/{name}"
  handler: (request, reply) ->
    reply "Hello, " + encodeURIComponent(request.params.name) + "!"
    
    
server.route do
  method: 'GET'
  path: '/static/{filename*}'
  handler: 
    file: (request) ->
      return './static/' + request.params.filename

server.views do
  engines:
    html:
      module: require("handlebars")
      compileMode: "sync" # engine specific
      isCached: false

  compileMode: "async" # global setting
  basePath: __dirname
  path: "./views"
  partialsPath: "./views/partials"
    
x = 1
y = 1
->
  x = 2
  y = 2
  console.log x, y
  
console.log x, y

a = require './static/weblib'
a.test!


server.start ->
  console.log "Server running at:", server.info.uri
  return
